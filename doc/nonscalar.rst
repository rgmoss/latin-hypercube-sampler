.. _nonscalar:

Non-scalar parameters
---------------------

.. note:: You should be familiar with `indexing`_ and `broadcasting`_ numpy arrays.

Non-scalar samples
^^^^^^^^^^^^^^^^^^

To draw samples for non-scalar parameters, you need to specify which dimensions should be uncorrelated.

For example, consider a parameter ``alpha`` with dimensions :math:`2 \times 3`  (i.e., one sample of this parameter contains 6 values).
There are four different ways in which you can draw samples for this parameter:

1. All 6 values are correlated, and should be drawn at the same quantiles:

   .. literalinclude:: ../tests/test_nonscalar.py
      :pyobject: test_alpha_1

2. Values in the first dimension (length 2) are correlated:

   .. literalinclude:: ../tests/test_nonscalar.py
      :pyobject: test_alpha_2

3. Values in the second dimension (length 3) are correlated:

   .. literalinclude:: ../tests/test_nonscalar.py
      :pyobject: test_alpha_3

4. None of the values are correlated:

   .. literalinclude:: ../tests/test_nonscalar.py
      :pyobject: test_alpha_4

Non-scalar shape parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can use non-scalar shape parameters, but they must be consistent with the desired samples shape.
In the example below, we draw samples for several parameters with dimensions :math:`2 \times 3`.
Note that the two shape parameters have dimensions :math:`2 \times 1` and :math:`1 \times 3`, and so they can both be broadcast to :math:`2 \times 3`.

   .. literalinclude:: ../tests/test_nonscalar.py
      :pyobject: test_alpha_5
