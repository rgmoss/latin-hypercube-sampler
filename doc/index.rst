Latin hypercube sampler
=======================

Welcome to the lhs_ documentation.
This package implements `Latin hypercube sampling <https://en.wikipedia.org/wiki/Latin_hypercube_sampling>`__ in order to draw near-random samples of parameter values from multi-dimensional distributions.
This package is primarily intended for scenario modelling.

License
-------

The code is distributed under the terms of the BSD 3-Clause license (see
``LICENSE``), and the documentation is distributed under the terms of the
`Creative Commons BY-SA 4.0 license
<http://creativecommons.org/licenses/by-sa/4.0/>`_.

A simple example
----------------

.. code-block:: python

   >>> import lhs
   >>> import numpy as np
   >>> import scipy.stats
   >>> # Define normal distributions for alpha, beta, and gamma.
   >>> alpha = {'name': 'norm', 'args': {'loc': 5, 'scale': 1}}
   >>> beta = {'name': 'norm', 'args': {'loc': 10, 'scale': 2}}
   >>> gamma = {'distribution': scipy.stats.norm(loc=0, scale=0.1)}
   >>> # Collect the distributions in a dictionary.
   >>> distributions = {'alpha': alpha, 'beta': beta, 'gamma': gamma}
   >>> # Define the number of samples to draw.
   >>> num_samples = 100
   >>> # Create a pseudo-random number generator.
   >>> seed = 12345
   >>> rand = np.random.default_rng(seed=seed)
   >>> # Draw samples for all three parameters.
   >>> # This returns a dictionary of 1-D NumPy arrays.
   >>> samples = lhs.draw(rand, num_samples, distributions)
   >>> # Print the first sampled value for alpha.
   >>> print(samples['alpha'][0])
   4.538220677728063
   >>> # Collect the samples for each parameter into a 2-D array.
   >>> table = np.column_stack([values for values in samples.values()])
   >>> # Print the first five samples.
   >>> print(table[:5])
   [[ 4.53822068e+00  1.14024721e+01  6.26798628e-02]
    [ 3.48555342e+00  1.15887999e+01 -8.20221380e-02]
    [ 6.91828587e+00  1.34933849e+01  3.75658088e-04]
    [ 6.02953073e+00  1.44184693e+01 -1.67071425e-01]
    [ 2.08898887e+00  9.06176782e+00  2.83558279e-02]]
   >>> # Save the samples table to a text file.
   >>> # np.savetxt('samples.out', table)

A more detailed example
-----------------------

1. Define a distribution for each independent parameter (in this example, ``R0`` and ``eta``):

   .. literalinclude:: ../tests/test_alpha_m.py
      :pyobject: independent_params

   .. note:: See :ref:`distributions` for details about defining distributions.

2. Define the name and size of each dependent parameter (if any; in this example, ``alpha_m``):

   .. literalinclude:: ../tests/test_alpha_m.py
      :pyobject: dependent_params

3. Define the distribution for each dependent parameter (if any), given the sampled values for every independent parameter, in a separate function:

   .. literalinclude:: ../tests/test_alpha_m.py
      :pyobject: dependent_dists

4. Draw samples for all of the model parameters:

   .. literalinclude:: ../tests/test_alpha_m.py
      :pyobject: draw_samples

   .. note:: If there are no dependent parameters, you can omit the dependent parameter arguments and simply call:

         .. code:: python

            samples = lhs.draw(rng, num_samples, independent_params())

         See :ref:`sampling` for further details about drawing samples.

5. You can then use these samples to, e.g., define initial model parameters.

   .. literalinclude:: ../tests/test_alpha_m.py
      :pyobject: test_draw_samples

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   Home <self>
   install
   distributions
   sampling
   nonscalar

.. _dev-docs:

.. toctree::
   :maxdepth: 2
   :caption: Development

   changelog
   contributing
   release-process
