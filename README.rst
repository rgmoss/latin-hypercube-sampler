Latin hypercube sampler for scenario modelling
==================================================

|version| |docs| |tests| |coverage|

Description
-----------

A Latin hypercube sampling framework that is primarily intended to support scenario modelling.
See the `online documentation <https://lhs.readthedocs.io/>`_ for details.

License
-------

The code is distributed under the terms of the `BSD 3-Clause license <https://opensource.org/licenses/BSD-3-Clause>`_ (see
``LICENSE``), and the documentation is distributed under the terms of the
`Creative Commons BY-SA 4.0 license
<http://creativecommons.org/licenses/by-sa/4.0/>`_.

Installation
------------

To install the latest release::

    pip install lhs

To install the latest development version, clone this repository and run::

    pip install .

.. |version| image:: https://badge.fury.io/py/lhs.svg
   :alt: Latest version
   :target: https://pypi.org/project/lhs/

.. |docs| image::  https://readthedocs.org/projects/lhs/badge/
   :alt: Documentation
   :target: https://lhs.readthedocs.io/

.. |tests| image:: https://gitlab.unimelb.edu.au/rgmoss/latin-hypercube-sampler/badges/master/pipeline.svg
   :alt: Test cases
   :target: https://gitlab.unimelb.edu.au/rgmoss/latin-hypercube-sampler

.. |coverage| image:: https://gitlab.unimelb.edu.au/rgmoss/latin-hypercube-sampler/badges/master/coverage.svg
   :alt: Test coverage
   :target: https://gitlab.unimelb.edu.au/rgmoss/latin-hypercube-sampler
