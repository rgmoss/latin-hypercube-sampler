0.4.2 (2024-05-01)
------------------

* Enhancement: identify the cause of an unknown distribution.
  This may be useful if the user names a distribution and expects it to be found in a module other than ``scipy.stats``.

* Enhancement: support NumPy 2.0 and newer.
  This package is not affected by the breaking changes in NumPy 2.0.

0.4.1 (2023-01-20)
------------------

* Bug fix: correct ``lhs.dist.inverse_uniform`` so that it actually returns samples drawn from an inverse-uniform distribution.
  Previously, it returned samples drawn from a uniform distribution.

0.4.0 (2022-12-23)
------------------

* Breaking change: require Python 3.8 or newer.

* Breaking change: draw samples using the order in which the distributions are defined.

0.3.3 (2022-08-19)
------------------

* Enhancement: add the ``lhs.dist.inverse_uniform`` distribution.

0.3.2 (2022-01-30)
------------------

* Bug fix: correct the documentation example for ``lhs.dist.constant``.

* Enhancement: add a simpler example to the documentation home page.

0.3.1 (2021-12-13)
------------------

* Bug fix: use the correct subspace samples for dependent parameters.

0.3.0 (2021-12-07)
------------------

* Breaking change: remove ``lhs.Sampler``, this will be provided by pypfilt 0.7.

* Breaking change: remove ``lhs.LhsSamples``, this is no longer needed because pypfilt 0.7 saves all model prior samples.

* Breaking change: replace ``lhs.sample`` functions with ``lhs.sample.lhs_values`` and ``lhs.sample.draw`` (also available as ``lhs.draw``).

* Enhancement: non-scalar samples and shape parameters are now supported.

* Enhancement: add support for broadcasting samples to add extra dimensions.

* Enhancement: distributions can now be identified by name, by providing a frozen SciPy distribution, or by providing the percent point function (inverse of the CDF).

* Remove the pypfilt dependency; pypfilt 0.7 will depend on this package instead.

0.2.0 (2020-12-18)
------------------

* Implement an LHS sampler for pypfilt >= 0.7.0 (not yet released).

0.1.0 (2020-12-03)
------------------

* Initial release.
