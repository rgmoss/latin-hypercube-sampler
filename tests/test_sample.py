import numpy as np
import pytest
import scipy.stats

import lhs
import lhs.sample


def make_rng():
    return np.random.default_rng(seed=20201217)


def check_samples(n, samples):
    assert np.all(samples >= 0)
    assert np.all(samples <= 1)

    bin_edges = np.arange(0, 1, step=1 / n)
    for lower, upper in zip(bin_edges[:-1], bin_edges[1:]):
        assert np.any(np.logical_and(samples >= lower, samples <= upper))


def test_lhs_values_uniform():
    name = 'good_uniform'
    min_val = -2
    max_val = 2
    dist = {
        'name': 'uniform',
        'args': {
            'loc': min_val,
            'scale': max_val - min_val,
        },
    }
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    values = lhs.sample.lhs_values(name, dist, samples)
    assert np.allclose(values, np.array([-2, -1, 0, 1, 2]))


def test_lhs_values_extra_keys(caplog):
    name = 'extra_keys'
    dist = {
        'name': 'uniform',
        'args': {'loc': 0, 'scale': 1},
        'something_else': 'unexpected',
    }
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    _ = lhs.sample.lhs_values(name, dist, samples)
    expect_msg = 'Extra prior keys for {}:'.format(name)
    assert expect_msg in caplog.text


def test_lhs_values_invalid_dist():
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    name = 'invalid_dist'
    invalid_dist = {
        'name': 'does-not-exist',
        'args': {},
    }
    with pytest.raises(ValueError):
        _ = lhs.sample.lhs_values(name, invalid_dist, samples)

    invalid_dist = {
        'name': 123,
        'args': {},
    }
    with pytest.raises(ValueError):
        _ = lhs.sample.lhs_values(name, invalid_dist, samples)


def test_lhs_values_no_dist():
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    name = 'no_dist'
    no_dist = {
        'args': {},
    }
    with pytest.raises(ValueError):
        _ = lhs.sample.lhs_values(name, no_dist, samples)


def test_lhs_values_invalid_args():
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    name = 'invalid_args'
    invalid_args = {
        'name': 'uniform',
        'args': {'not_an_argument': 1},
    }
    with pytest.raises(TypeError):
        _ = lhs.sample.lhs_values(name, invalid_args, samples)

    invalid_args = {
        'name': 'uniform',
        'args': 123,
    }
    with pytest.raises(ValueError):
        _ = lhs.sample.lhs_values(name, invalid_args, samples)


def test_lhs_values_no_args():
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    name = 'no_args'
    no_args = {
        'name': 'uniform',
    }
    with pytest.raises(ValueError):
        _ = lhs.sample.lhs_values(name, no_args, samples)


def test_lhs_values_dist_types():
    """
    Ensure that identical values are obtained when defining the sampling
    distribution by name, or as a ``scipy.stats`` distribution, or as a
    function.
    """
    samples = np.array([0.0, 0.25, 0.50, 0.75, 1.0])
    dist_name = {
        'name': 'beta',
        'args': {'a': 2, 'b': 5},
    }
    dist_dist = {
        'distribution': scipy.stats.beta(a=2, b=5),
    }
    dist_ppf = {
        'ppf': scipy.stats.beta(a=2, b=5).ppf,
    }
    name_values = lhs.sample.lhs_values('dist_name', dist_name, samples)
    dist_values = lhs.sample.lhs_values('dist_dist', dist_dist, samples)
    ppf_values = lhs.sample.lhs_values('dist_ppf', dist_ppf, samples)
    assert np.allclose(name_values, dist_values)
    assert np.allclose(name_values, ppf_values)
    assert np.allclose(dist_values, ppf_values)


def test_subspace():
    rng = make_rng()

    for n in [1, 10, 100]:
        for shape in [None, 1, 2, 5]:
            samples = lhs.sample.sample_subspace(rng, n, shape)

            if shape is None or shape == 1:
                assert samples.shape == (n,)
            else:
                assert samples.shape == (n, shape)

            check_samples(n, samples)


def test_subspace_for_2x3_parameter():
    """
    Ensure that each element of a 2x3 parameter is sampled.
    """
    rng = np.random.default_rng(12345)
    num_samples = 100
    shape = (2, 3)
    samples = lhs.sample.sample_subspace(rng, num_samples, shape)
    assert samples.shape == (num_samples, 2, 3)
    # Ensure each subarray contains one value in every interval.
    for a in range(2):
        for b in range(3):
            for c in range(num_samples):
                in_interval = np.logical_and(
                    samples[:, a, b] >= c / num_samples,
                    samples[:, a, b] <= (c + 1) / num_samples,
                )
                assert sum(in_interval) == 1


def test_subspaces():
    rng = make_rng()
    n = 50
    params = [('a', 1), ('b', 2), ('c', None)]
    tbl = lhs.sample.sample_subspaces(rng, n, params)

    assert set(tbl.keys()) == {'a', 'b', 'c'}
    assert tbl['a'].shape == tbl['c'].shape
    assert tbl['a'].shape != tbl['b'].shape

    for _name, samples in tbl.items():
        check_samples(n, samples)

    # Ensure the samples for each parameter are not perfectly correlated.
    ixs_a = np.argsort(tbl['a'])
    ixs_b1 = np.argsort(tbl['b'][..., 0])
    ixs_b2 = np.argsort(tbl['b'][..., 1])
    ixs_c = np.argsort(tbl['c'])
    assert not np.all(ixs_a == ixs_b1)
    assert not np.all(ixs_a == ixs_b2)
    assert not np.all(ixs_a == ixs_c)
    assert not np.all(ixs_b1 == ixs_b2)
    assert not np.all(ixs_b1 == ixs_c)
    assert not np.all(ixs_b2 == ixs_c)


def test_subspaces_invalid_type():
    n = 50
    rng = make_rng()
    with pytest.raises(ValueError):
        lhs.sample.sample_subspaces(rng, n, 'Hello World')
    with pytest.raises(ValueError):
        lhs.sample.sample_subspaces(rng, n, [('a', 'b')])
