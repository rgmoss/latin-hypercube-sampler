import lhs
import numpy as np
import scipy.stats


def independent_params():
    return {
        # Basic reproduction number.
        'R0': {
            'name': 'constant',
            'args': {'value': 2.53},
            'shape': 1,
        },
        # Proportion of infections that require hospitalisation ("severe").
        # See doi:10.1371/journal.pone.0014505 for details.
        'eta': {
            'distribution': scipy.stats.loguniform(a=0.05, b=0.1),
            'shape': 1,
        },
    }


def dependent_params():
    return {
        # Proportion of non-severe infections that seek clinical care.
        # See doi:10.1371/journal.pone.0014505 for details.
        'alpha_m': {'shape': 1},
    }


def dependent_dists(indep_values, dep_params):
    # Scale eta from [0.001, 0.1] to [0.2, 1.0]
    eta = indep_values['eta']
    scale = (100 * eta - 0.1) * 0.8 / 9.9 + 0.2
    # Calculate the mean and bounds for alpha_m.
    mean_val = scale * 0.5
    min_val = scale * 0.25
    max_val = scale * 0.75
    # Calculate the mean and variance for alpha_m.
    varn = scale * 0.1
    span = max_val - min_val
    scaled_mean = (mean_val - min_val) / span
    scaled_var = varn / span
    # Calculate the shape parameters for alpha_m.
    a = np.square(scaled_mean / scaled_var) * (1 - scaled_mean) - scaled_mean
    b = a * (1 - scaled_mean) / scaled_mean

    dist = scipy.stats.beta(a=a, b=b, loc=min_val, scale=span)

    return {
        'alpha_m': {'distribution': dist},
    }


def draw_samples(seed, num_samples):
    rng = np.random.default_rng(seed=seed)
    samples = lhs.draw(
        rng,
        num_samples,
        independent_params(),
        dependent_params(),
        dependent_dists,
    )
    return samples


def test_draw_samples():
    samples = draw_samples(seed=12345, num_samples=1000)
    # Ensure that R0 is constant.
    assert all(samples['R0'] == 2.53)
    # Ensure that all eta values lie within the interval bounds.
    assert all(samples['eta'] > 0.05)
    assert all(samples['eta'] < 0.10)
    # Ensure that between 5% and 75% of infections lead to non-severe clinical
    # presentations.
    assert all(samples['alpha_m'] > 0.05)
    assert all(samples['alpha_m'] < 0.75)
