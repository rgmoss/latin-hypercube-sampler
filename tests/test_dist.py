import numpy as np
import pytest
import scipy.stats

import lhs.dist


def make_rng():
    return np.random.default_rng(seed=20201217)


def test_constant():
    rng = make_rng()
    for x in [1, 5, 10]:
        for y in [1, 2, 3]:
            want_shape = (x, y)
            for const_val in [1e-4, np.pi, 200]:
                samples = rng.random(want_shape)
                values = lhs.dist.constant(samples, const_val)
                assert values.shape == want_shape
                assert np.all(values == const_val)
                # Check that we obtain the same results from the top-level
                # function.
                kwargs = {'value': const_val}
                values2 = lhs.dist.sample_from(samples, 'constant', kwargs)
                assert np.array_equal(values, values2)


def test_inverse_uniform():
    rng = make_rng()
    for low in [1, 2, 3]:
        for high in [5, 10, 20]:
            want_shape = (low, high)
            samples = rng.random(want_shape)
            values = lhs.dist.inverse_uniform(samples, low=low, high=high)
            # Check that we obtain values within the expected bounds.
            assert values.shape == want_shape
            assert np.all(values >= 1 / high)
            assert np.all(values <= 1 / low)
            # Check that there is variation across the values.
            assert np.min(values) < np.max(values)
            # Check that we obtain the same results from the top-level
            # function, for each of the four parameter combinations.
            kwargs = {'low': low, 'high': high}
            values2 = lhs.dist.sample_from(samples, 'inverse_uniform', kwargs)
            assert np.array_equal(values, values2)
            kwargs = {'inv_low': 1 / low, 'high': high}
            values3 = lhs.dist.sample_from(samples, 'inverse_uniform', kwargs)
            assert np.allclose(values, values3)
            kwargs = {'low': low, 'inv_high': 1 / high}
            values4 = lhs.dist.sample_from(samples, 'inverse_uniform', kwargs)
            assert np.allclose(values, values4)
            kwargs = {'inv_low': 1 / low, 'inv_high': 1 / high}
            values5 = lhs.dist.sample_from(samples, 'inverse_uniform', kwargs)
            assert np.allclose(values, values5)


def test_inverse_uniform_invalid_args():
    samples = [0.1, 0.5, 0.9]
    with pytest.raises(ValueError):
        lhs.dist.sample_from(samples, 'inverse_uniform', {})
    with pytest.raises(ValueError):
        lhs.dist.sample_from(samples, 'inverse_uniform', {'a': 1})
    with pytest.raises(ValueError):
        lhs.dist.sample_from(samples, 'inverse_uniform', {'a': 1, 'b': 2})
    with pytest.raises(ValueError):
        lhs.dist.sample_from(samples, 'inverse_uniform', {'low': 1, 'b': 2})
    with pytest.raises(ValueError):
        lhs.dist.sample_from(samples, 'inverse_uniform', {'a': 1, 'high': 2})
    with pytest.raises(ValueError):
        lhs.dist.sample_from(
            samples, 'inverse_uniform', {'low': 1, 'inv_low': 1}
        )
    with pytest.raises(ValueError):
        lhs.dist.sample_from(
            samples, 'inverse_uniform', {'high': 2, 'inv_high': 2}
        )
    with pytest.raises(ValueError):
        lhs.dist.sample_from(
            samples, 'inverse_uniform', {'low': 1, 'high': 2, 'extra': 3}
        )


def test_beta():
    rng = make_rng()

    beta_min = 10
    beta_max = 12
    beta_range = beta_max - beta_min
    delta = 0.05 * beta_range

    want_shape = (500,)
    # NOTE: we're not testing the lhs.sample module here, so rather than
    # drawing a sample from each of `n` equal subsets of the unit interval,
    # we're simply drawing `n` samples from the unit interval.
    samples = rng.random(want_shape)

    for a in [0.5, 1, 2]:
        for b in [1, 10]:
            kwargs = {
                'a': a,
                'b': b,
                'loc': beta_min,
                'scale': beta_range,
            }
            values = lhs.dist.scipy_stats_dist(samples, 'beta', kwargs)

            # Ensure that we get the expected number of values, and they all
            # fall within the expected interval.
            assert values.shape == want_shape
            assert np.all(values >= beta_min)
            assert np.all(values <= beta_max)

            # Ensure that the extreme values are consistent with the extreme
            # samples from the unit interval.
            dist = scipy.stats.beta(**kwargs)
            assert np.isclose(min(samples), dist.cdf(min(values)))
            assert np.isclose(max(samples), dist.cdf(max(values)))

            # Ensure the sample mean is a reasonable approximation of the true
            # mean.
            sample_mean = np.mean(values)
            unit_mean = kwargs['a'] / (kwargs['a'] + kwargs['b'])
            true_mean = beta_min + beta_range * unit_mean
            assert np.abs(true_mean - sample_mean) < delta

            # Check that we obtain the same results from the top-level
            # function.
            values2 = lhs.dist.sample_from(samples, 'beta', kwargs)
            assert np.array_equal(values, values2)


def test_unknown_dist():
    rng = make_rng()
    samples = rng.random(3)
    with pytest.raises(ValueError):
        _ = lhs.dist.sample_from(samples, 'unknown_dist', {})


def test_invalid_dist():
    rng = make_rng()
    samples = rng.random(3)
    with pytest.raises(ValueError):
        _ = lhs.dist.sample_from(samples, '__doc__', {})


def test_fully_qualified_dist_name():
    const_val = 42
    num_samples = 5
    dist_name = 'lhs.dist.constant'
    dist_kwargs = {'value': const_val}
    rng = make_rng()
    samples = rng.random(num_samples)
    values = lhs.dist.sample_from(samples, dist_name, dist_kwargs)
    assert len(values) == num_samples
    assert all(values == const_val)
